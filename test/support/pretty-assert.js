'use strict';

var assert = require('assert'),
    util = require('util');

var assertionMethods = ['equal', 'notEqual', 'deepEqual', 'notDeepEqual', 'strictEqual', 'notStrictEqual', 'throws', 'doesNotThrow'];

var prettyAssert = module.exports = function prettyAssert() {
  assert.ok.apply(assert, arguments);
};

prettyAssert['ok'] = assert['ok'];
prettyAssert['fail'] = assert['fail'];
prettyAssert['ifError'] = assert['ifError'];

assertionMethods.forEach(function(value) {
  prettyAssert[value] = function() {
    if(value.match(/throw/i)) {
      assert[value].apply(assert, arguments);
    } else {
      var args = [arguments['0']];
      if(typeof arguments['1'] !== 'undefined') {
        args.push(arguments['1'],
          (arguments['2'] || '') + '\n\tActual:   \u001b[0m' + util.inspect(arguments['0'], false, 5, true).replace(/\n/g, '\n\t\t')
          + '\n\t  \u001b[31m' + value + '\n\tExpected: \u001b[0m' + util.inspect(arguments['1'], false, 5, true).replace(/\n/g, '\n\t\t'));
      }
      assert[value].apply(assert, args);
    }
  };
});