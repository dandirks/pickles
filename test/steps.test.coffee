assert = require('./support/pretty-assert')
util = require('util')
#replace the default zombie used in files with a mocked version
require('./mocks/zombie-mock')
Acceptance = require('../lib/acceptance')

suite 'steps.coffee', ->
  setup ->
    @steps = require('../lib/steps') #SUT
    @definedSteps = {}
    @mockCucumber =
      World: null
      defineStep: (key, method) => @definedSteps[key] = method

  test 'should use default acceptance if none supplied', ->
    @steps.call(@mockCucumber)

    assert.strictEqual(Acceptance, @mockCucumber.World)

  test 'should use custom acceptance if supplied', ->
    myAcceptance = -> return
    util.inherits(myAcceptance, Acceptance)
    @steps.call(@mockCucumber, myAcceptance)

    assert.notStrictEqual(Acceptance, @mockCucumber.World)
    assert.strictEqual(myAcceptance, @mockCucumber.World)

  test 'should define the appropriate steps', ->
    expected =
      """/^I visit the "?([^"]+)"? page$/""": Acceptance::visit
      """/^I am on the "?([^"]+)"? page$/""": Acceptance::verifyCurrentPage
      """/^I see the text "?([^"]+)"?$/""": Acceptance::verifyTextOnPage
      """/^I click the "?([^"]+)"? button$/""": Acceptance::clickButton
      """/^the "?([^"]+)"? field does not exist$/""": Acceptance::verifyFieldDoesntExist
      """/^I enter "?([^"]+)"? in the "?([^"]+)"? field$/""": Acceptance::enterTextInField
      """/^I see "?([^"]+)"? in the "?([^"]+)"? field$/""": Acceptance::verifyTextInField
      """/^I (check|uncheck) the "?([^"]*)"? checkbox$/""": Acceptance::toggleCheckboxState
      """/^I see the "?([^"]*)"? checkbox is (checked|unchecked)$/""": Acceptance::verifyCheckboxState
      """/^I see an alert saying "?([^"]*)"?$/""": Acceptance::verifyAlert

    @steps.call(@mockCucumber)
    assert.deepEqual(expected, @definedSteps, """Expected: #{util.inspect(expected, false, 1, true)} Got: #{util.inspect(@definedSteps, false, 1, true)}""")
