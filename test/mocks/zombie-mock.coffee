class ZombieBrowserMock
  constructor: ->
  visit: ->
  query: ->
  queryAll: ->
  xpath: ->
  fill: ->
  fire: ->
  check: ->
  uncheck: ->
  prompted: ->



require.cache[require.resolve('zombie')] =
  exports: ZombieBrowserMock
