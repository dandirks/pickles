assert = require('./support/pretty-assert')
util = require('util')
sinon = require('sinon')
#replace the default zombie used in files with a mocked version
require('./mocks/zombie-mock')
zombie = require('zombie')
World = require('../lib/worlds/zombie-world').World


suite 'zombie-world.coffee', ->

  test 'should create new Browser object with correct state', ->
    callback = sinon.spy()
    world = Object.create(null)

    World.call(world, callback)

    assert(world.browser instanceof zombie, 'zombie.Browser not defined correctly')
    assert.equal(world.browser.site, 'localhost:9500', 'site not set correctly')
    assert.deepEqual(world.by, { css: 'css', xpath: 'xpath', label: 'label' }, '@by not set correctly')
    assert(callback.calledOnce, 'callback not called')

  test 'should use environment variables to set site value', ->
    callback = sinon.spy()
    world = Object.create(null)
    server = process.env.SERVER = 'my_server'
    port = process.env.PORT = '10000'

    World.call(world, callback)

    assert.equal(world.browser.site, "#{server}:#{port}", 'site not set correctly')


  suite 'api', ->
    setup ->
      @World = Object.create(null)
      World.call(@World, ->)
      @browserMock = sinon.mock(@World.browser)

    teardown ->
      @browserMock.verify()

    suite 'visit', ->
      test 'should visit the correct page', ->
        url = 'http://test.url'
        callback = ->

        @browserMock.expects('visit').once().withExactArgs(url, callback)

        @World.visit(url, callback)


    suite 'path', ->
      test 'should return the current path', ->
        path = '/my/awesome/path'
        @World.browser.location = { pathname: path }

        actual = @World.path()

        assert.equal(actual, path, 'path not returned correctly')

        delete @World.browser.location


    suite 'findElement', ->
      test 'should return a single element using a CSS selector', ->
        selector = '.my-element'
        findBy = 'css'
        findAll = false
        element = Object.create(null)

        @browserMock.expects('query').once().on(@World.browser).withExactArgs(selector).returns(element)

        actual = @World.findElement(selector, findBy, findAll)

        assert.strictEqual(actual, element, 'element not returned')

      test 'should return a single element using a label selector', ->
        selector = '.my-label'
        findBy = 'label'
        findAll = false
        getAttributeStub = sinon.stub()
        label = { getAttribute: getAttributeStub }
        labelForText = 'my-element'
        element = Object.create(null)

        @browserMock.expects('query').once().withExactArgs("label:contains('#{selector}')").returns(label)
        getAttributeStub.withArgs('for').returns(labelForText)
        @browserMock.expects('query').once().withExactArgs("##{labelForText}").returns(element)

        actual = @World.findElement(selector, findBy, findAll)

        assert.strictEqual(actual, element, 'element not returned')

      test 'should return a single element using an xpath selector', ->
        selector = '//div[contains(@class, "my-element"]'
        findBy = 'xpath'
        findAll = false
        element = Object.create(null)
        xpathReturn = { value: [element] }

        @browserMock.expects('xpath').once().withExactArgs(selector).returns(xpathReturn)

        actual = @World.findElement(selector, findBy, findAll)

        assert.strictEqual(actual, element, 'element not returned')

      test 'should return multiple elements using a CSS selector', ->
        selector = '.my-element'
        findBy = 'css'
        findAll = true
        element1 = Object.create(null)
        element2 = Object.create(null)
        element3 = Object.create(null)
        elementList = [element1, element2, element3]

        @browserMock.expects('queryAll').once().on(@World.browser).withExactArgs(selector).returns(elementList)

        actual = @World.findElement(selector, findBy, findAll)

        assert.strictEqual(actual, elementList, 'element list not returned')

      test 'should return a single element using a label selector even if asking for multiple', ->
        selector = '.my-label'
        findBy = 'label'
        findAll = true
        getAttributeStub = sinon.stub()
        label = { getAttribute: getAttributeStub }
        labelForText = 'my-element'
        element = Object.create(null)

        @browserMock.expects('query').once().withExactArgs("label:contains('#{selector}')").returns(label)
        getAttributeStub.withArgs('for').returns(labelForText)
        @browserMock.expects('query').once().withExactArgs("##{labelForText}").returns(element)

        actual = null
        assert.doesNotThrow (=>
          actual = @World.findElement(selector, findBy, findAll))
        /Element '.my-label' not found using search method label/

        assert.strictEqual(actual, element, 'element not returned')

      test 'should return multiple elements using an xpath selector', ->
        selector = '//div[contains(@class, "my-element"]'
        findBy = 'xpath'
        findAll = true
        element1 = Object.create(null)
        element2 = Object.create(null)
        element3 = Object.create(null)
        elementList = [element1, element2, element3]
        xpathReturn = { value: elementList }

        @browserMock.expects('xpath').once().withExactArgs(selector).returns(xpathReturn)

        actual = @World.findElement(selector, findBy, findAll)

        assert.strictEqual(actual, elementList, 'element list not returned')

      test 'should throw an error if element not found using a CSS selector', ->
        selector = '.my-element'
        findBy = 'css'
        findAll = false

        @browserMock.expects('query').once().on(@World.browser).withExactArgs(selector).returns(undefined)

        assert.throws (=>
          @World.findElement(selector, findBy, findAll)),
        /Element '.my-element' not found using search method css/

      test 'should throw an error if label not found using a label selector', ->
        selector = '.my-element'
        findBy = 'label'
        findAll = false

        @browserMock.expects('query').once().withExactArgs("label:contains('#{selector}')").returns(undefined)

        assert.throws (=>
          @World.findElement(selector, findBy, findAll)),
        /Element '.my-element' not found using search method label/

      test 'should throw an error if element not found using a label selector', ->
        selector = '.my-element'
        findBy = 'label'
        findAll = false
        getAttributeStub = sinon.stub()
        label = { getAttribute: getAttributeStub }
        labelForText = 'my-element'

        @browserMock.expects('query').once().withExactArgs("label:contains('#{selector}')").returns(label)
        getAttributeStub.withArgs('for').returns(labelForText)
        @browserMock.expects('query').once().withExactArgs("##{labelForText}").returns(undefined)

        assert.throws (=>
          @World.findElement(selector, findBy, findAll)),
        /Element '.my-element' not found using search method label/

      test 'should throw an error if element not found using an xpath selector', ->
        selector = '.my-element'
        findBy = 'xpath'
        findAll = false

        @browserMock.expects('xpath').once().withExactArgs(selector).returns(undefined)

        assert.throws (=>
          @World.findElement(selector, findBy, findAll)),
        /Element '.my-element' not found using search method xpath/


    suite 'type', ->
      test 'should type in the correct input box and fire events', ->
        field = '.my-input'
        value = 'some awesome text!'
        thenStub = sinon.stub()
        thenReturn = { then: thenStub }
        callback = sinon.spy()

        @browserMock.expects('fill').once().withArgs(field, value).yields()
        @browserMock.expects('fire').once().withExactArgs('keydown', field).returns(thenReturn)
        @browserMock.expects('fire').once().withExactArgs('keyup', field).returns(thenReturn)
        @browserMock.expects('fire').once().withExactArgs('keypress', field).returns(thenReturn)
        thenStub.returns(thenReturn).yields()

        @World.type(field, value, callback)

        assert(callback.calledOnce, 'callback not called')


    suite 'click', ->
      test 'should fire a click event for the element', ->
        element = Object.create(null)
        callback = ->

        @browserMock.expects('fire').once().withExactArgs('click', element, callback)

        @World.click(element, callback)


    suite 'check', ->
      test 'should check an element', ->
        element = Object.create(null)
        callback = ->

        @browserMock.expects('check').once().withExactArgs(element, callback)

        @World.check(element, callback)


    suite 'uncheck', ->
      test 'should uncheck an element', ->
        element = Object.create(null)
        callback = ->

        @browserMock.expects('uncheck').once().withExactArgs(element, callback)

        @World.uncheck(element, callback)


    suite 'alertedWithMessage', ->
      test 'should return true if browser received an alert with message', ->
        message = 'UHOH!'

        @browserMock.expects('prompted').once().withExactArgs(message).returns(true)

        actual = @World.alertedWithMessage(message)

        assert.equal(actual, true, 'alertedWithMessage should have returned true')

      test 'should return false if browser did not receive an alert with message', ->
        message = 'UHOH!'

        @browserMock.expects('prompted').once().withExactArgs(message).returns(false)

        actual = @World.alertedWithMessage(message)

        assert.equal(actual, false, 'alertedWithMessage should have returned false')
