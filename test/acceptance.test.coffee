assert = require('./support/pretty-assert')
util = require('util')
sinon = require('sinon')
#replace the default zombie used in files with a mocked version
require('./mocks/zombie-mock')
Acceptance = require('../lib/acceptance')


suite 'acceptance.coffee', ->
  setup (done) ->
    callback = =>
      process.nextTick =>
        @mockWorld = sinon.mock(@acceptance.World)
        done()
    @acceptance = new Acceptance(callback)

  teardown ->
    @mockWorld.verify()
    Acceptance::pages = {}


  suite 'constructor', ->
    test 'should create a new world based on included world when constructed', (done) ->
      callback = =>
        process.nextTick =>
          assert(typeof myAcceptance.World is 'object')
          done()
      myAcceptance = new Acceptance(callback)


  suite 'visit', ->
    test 'should visit correct page if page is defined in Acceptance::pages', ->
      Acceptance::pages =
        'Home': '/'
        'NotMyHome': '/jerkshome'

      callback = sinon.spy()
      @mockWorld.expects('visit').once().withExactArgs('/', callback)

      @acceptance.visit('Home', callback)

    test 'should visit correct page if matching regex page is defined in Acceptance::pages', ->
      Acceptance::pages =
        '(My)?Home': '/myhome'
        'Not(My)?Home': '/yourhome'

      callback = sinon.spy()
      @mockWorld.expects('visit').once().withExactArgs('/myhome', callback)

      @acceptance.visit('Home', callback)

    test 'should throw an error if trying to visit undefined page', ->
      assert.throws (=>
        @acceptance.visit('MyAwesomePage', ->)),
      /Page 'MyAwesomePage' not defined/


  suite 'enterTextInField', ->
    test 'should type text in correct field', ->
      text = 'pants'
      field = 'jeans'
      element = Object.create(null)
      callback = sinon.spy()
      @mockWorld.expects('findElement').once().withExactArgs("input[name='#{field}'], textarea[name='#{field}']").returns(element)
      @mockWorld.expects('type').once().withExactArgs(element, text, callback)

      @acceptance.enterTextInField(text, field, callback)


  suite 'clickButton', ->
    test 'should click on correct button', ->
      button = 'pants'
      element = Object.create(null)
      callback = sinon.spy()
      @mockWorld.expects('findElement').once().withExactArgs("button:contains('#{button}')").returns(element)
      @mockWorld.expects('click').once().withExactArgs(element, callback)

      @acceptance.clickButton(button, callback)


  suite 'toggleCheckboxState', ->
    test 'should check checkbox when state is check', ->
      state = 'check'
      checkbox = 'pants'
      element = Object.create(null)
      callback = sinon.spy()
      @mockWorld.expects('findElement').once().withExactArgs(checkbox, @acceptance.World.by.label).returns(element)
      @mockWorld.expects('check').once().withExactArgs(element, callback)

      @acceptance.toggleCheckboxState(state, checkbox, callback)

    test 'should uncheck checkbox when state is uncheck', ->
      state = 'uncheck'
      checkbox = 'pants'
      element = Object.create(null)
      callback = sinon.spy()
      @mockWorld.expects('findElement').once().withExactArgs(checkbox, @acceptance.World.by.label).returns(element)
      @mockWorld.expects('uncheck').once().withExactArgs(element, callback)

      @acceptance.toggleCheckboxState(state, checkbox, callback)


  suite 'verifyCurrentPage', ->
    test 'should not fail if the current path matches page', ->
      Acceptance::pages =
        '(My)?Home': '/myhome'
        'Not(My)?Home': '/yourhome'
      callback = sinon.spy()
      @mockWorld.expects('path').once().returns('/myhome')

      @acceptance.verifyCurrentPage('MyHome', callback)
      assert(callback.called)

    test 'should fail if the current path does not match page', ->
      Acceptance::pages =
        '(My)?Home': '/myhome'
        'Not(My)?Home': '/yourhome'
      callback = sinon.spy()
      @mockWorld.expects('path').once().returns('/yourhome')

      assert.throws (=>
        @acceptance.verifyCurrentPage('MyHome', callback)),
      /Not on 'MyHome' page/
      assert(not callback.called)

    test 'should fail if required page is unknown', ->
      Acceptance::pages =
        'Not(My)?Home': '/yourhome'
      callback = sinon.spy()
      @mockWorld.expects('path').once().returns('/yourhome')

      assert.throws (=>
        @acceptance.verifyCurrentPage('MyHome', callback)),
      /Page 'MyHome' not defined/
      assert(not callback.called)


  suite 'verifyTextOnPage', ->
    test 'should not fail if text exists on the page', ->
      bodyText = """
        This is some AWESOME text!
        It has new lines and everything!
        ERMAHGERD!
      """
      element = Object.create(null, { textContent: { value: bodyText } })
      callback = sinon.spy()
      @mockWorld.expects('findElement').once().withExactArgs('body').returns(element)

      @acceptance.verifyTextOnPage("everything!\nERMAHGERD!", callback)
      assert(callback.called)

    test 'should fail if text does not exist on the page', ->
      bodyText = """
        This is some AWESOME text!
        It has new lines and everything!
        ERMAHGERD!
      """
      element = Object.create(null, { textContent: { value: bodyText } })
      callback = sinon.spy()
      @mockWorld.expects('findElement').once().withExactArgs('body').returns(element)

      assert.throws (=>
        @acceptance.verifyTextOnPage("ERMAHGERD!!!!!!1!", callback)),
      /Text 'ERMAHGERD!!!!!!1!' not found on page/
      assert(not callback.called)


  suite 'verifyFieldDoesntExist', ->
    test 'should not fail if field does not exist on the page', ->
      field = 'pants'
      callback = sinon.spy()
      @mockWorld.expects('findElement').once().withExactArgs("input[name='#{field}'], textarea[name='#{field}']").throws()

      @acceptance.verifyFieldDoesntExist(field, callback)
      assert(callback.called)

    test 'should fail if field exists on the page', (done) ->
      field = 'pants'
      callback = sinon.spy()
      callback.fail = (error) ->
        assert.equal("Field '#{field}' found on page.", error.message)
        assert(not callback.called)
        done()

      @mockWorld.expects('findElement').once().withExactArgs("input[name='#{field}'], textarea[name='#{field}']")

      @acceptance.verifyFieldDoesntExist(field, callback)


  suite 'verifyTextInField', ->
    test 'should not fail if field contains text', ->
      text = 'foo'
      field = 'pants'
      element = Object.create(null, { value: { value: text } })
      callback = sinon.spy()
      @mockWorld.expects('findElement').once().withExactArgs("input[name='#{field}'], textarea[name='#{field}']").returns(element)

      @acceptance.verifyTextInField(text, field, callback)
      assert(callback.called)

    test 'should fail if field does not contain text', ->
      text = 'foo'
      field = 'pants'
      element = Object.create(null, { value: { value: 'nope' } })
      callback = sinon.spy()
      @mockWorld.expects('findElement').once().withExactArgs("input[name='#{field}'], textarea[name='#{field}']").returns(element)

      assert.throws (=>
        @acceptance.verifyTextInField(text, field, callback)),
      /Text 'foo' not found in field 'pants'/
      assert(not callback.called)


  suite 'verifyCheckboxState', ->
    test 'should not fail if expected is checked and field is checked', ->
      checkbox = 'foo'
      state = 'checked'
      element = Object.create(null, { checked: { value: true } })
      callback = sinon.spy()
      @mockWorld.expects('findElement').once().withExactArgs(checkbox, @acceptance.World.by.label).returns(element)

      @acceptance.verifyCheckboxState(checkbox, state, callback)
      assert(callback.called)

    test 'should not fail if expected is unchecked and field is unchecked', ->
      checkbox = 'foo'
      state = 'unchecked'
      element = Object.create(null, { checked: { value: false } })
      callback = sinon.spy()
      @mockWorld.expects('findElement').once().withExactArgs(checkbox, @acceptance.World.by.label).returns(element)

      @acceptance.verifyCheckboxState(checkbox, state, callback)
      assert(callback.called)

    test 'should fail if expected is checked and field is unchecked', ->
      checkbox = 'foo'
      state = 'checked'
      element = Object.create(null, { checked: { value: false } })
      callback = sinon.spy()
      @mockWorld.expects('findElement').once().withExactArgs(checkbox, @acceptance.World.by.label).returns(element)

      assert.throws (=>
        @acceptance.verifyCheckboxState(checkbox, state, callback)),
      /Checkbox was not checked/
      assert(not callback.called)

    test 'should fail if expected is unchecked and field is checked', ->
      checkbox = 'foo'
      state = 'unchecked'
      element = Object.create(null, { checked: { value: true } })
      callback = sinon.spy()
      @mockWorld.expects('findElement').once().withExactArgs(checkbox, @acceptance.World.by.label).returns(element)

      assert.throws (=>
        @acceptance.verifyCheckboxState(checkbox, state, callback)),
      /Checkbox was not unchecked/
      assert(not callback.called)


  suite 'verifyAlert', ->
    test 'should not fail if alerted with correct message', ->
      message = 'foo bar pants sock'
      callback = sinon.spy()
      @mockWorld.expects('alertedWithMessage').once().withExactArgs(message).returns(true)

      @acceptance.verifyAlert(message, callback)
      assert(callback.called)

    test 'should fail if alerted not with correct message', ->
      message = 'foo bar pants sock'
      callback = sinon.spy()
      @mockWorld.expects('alertedWithMessage').once().withExactArgs(message).returns(false)

      assert.throws (=>
        @acceptance.verifyAlert(message, callback)),
      /Alert with text 'foo bar pants sock' not seen on page./
      assert(not callback.called)
