var Acceptance = require('./lib/acceptance');

module.exports = {
  'Acceptance': Acceptance,
  'defineSteps': require('./lib/steps')
};

Object.defineProperty(module.exports, 'pages', {
  get: function() {
    return Acceptance.prototype.pages;
  },
  set: function(value) {
    Acceptance.prototype.pages = value;
  },
  enumerable: true
});