# Pickles!
Pickles is intended to be a starting point for writing acceptance tests using [Cucumber.js](https://github.com/cucumber/cucumber-js).
Currently, Pickles only works with [Zombie.js](http://zombie.labnotes.org/) for running tests, but more test engines will be added in the future.

## How it works
Pickles provides three different interfaces that can be overriden. These interfaces define various steps to be executed
by Cucumber, the implementation of those steps, and the communciation layer with Zombie.

#### Steps
Steps are the glue between the Gherkin acceptance files and Cucumber. Cucumber parses the Gherkin files and executes any
steps that match. Pickles provides a number of handy, generic steps that you can utilize. *Insert link to Wiki here*

In order to start writing and running acceptance tests with Cucumber and Pickles, you must override the steps
implementation. The most basic way to do this is to include Pickles and provide that to Cucumber, as shown below:

    :::coffeescript
    #/tests/features/steps/steps.coffee
    pickles = require('pickles')

    module.exports = pickles.defineSteps

This will create a file that merely mirror's Pickles steps without adding any additional functionality. This file (which
we recommend calling steps.coffee or steps.js) will need to be passed into Cucumber as it's step definition, like so:

    :::shell
    $ cucumber.js features/my_feature.feature --require features/steps/steps.coffee

#### Acceptance
Step implementations are stored in an object called Acceptance. When passed into Cucumber as World, Cucumber will
automatically create a new Acceptance object at the start of tests. In order to add new steps to Cucumber, we recommend
that you create a new acceptance.coffee or acceptance.js file and inherit from Pickle's Acceptance object, like so:

    :::coffeescript
    #/tests/features/support/acceptance.coffee
    Acceptance = require('pickles').Acceptance

    class MyWebsiteAcceptance extends Acceptance

      ##
      # Creates a new acceptance object.
      #
      # @constructor
      # @this {MyWebsiteAcceptance}
      # @param {Function} callback The callback to be executed once creation is complete.
      #
      constructor: (callback) ->
        super(callback)

      ##
      # Does something SUPER AWESOMELY!
      #
      # @this {MyWebsiteAcceptance}
      # @param {String} something The thing to be done AWESOMELY!
      # @param {Function} callback The callback to be executed after completion.
      #
      mySuperAwesomeStep: (something, callback) ->
        console.log("#{something} IS AWESOME!")
        callback()

    module.exports = MyWebsiteAcceptance

This creates a new Acceptance object called MyWebsiteAcceptance which implements all of Acceptance but adds a
mySuperAwesomeStep method. We can use this method inside of our steps file to implement a new Gherkin step:

    :::coffeescript
    #/tests/features/steps/steps.coffee
    pickles = require('pickles')

    module.exports = ->
      #load in our custom MyWebsiteAcceptance
      Acceptance = require('../support/acceptance')
      #allow pickles to define all default steps using our custom Acceptance object
      pickles.defineSteps.call(@, Acceptance)

      #define our new step
      @defineStep(/^I DO "?([^"]*)"? AWESOMELY!$/, Acceptance::mySuperAwesomeStep)

This can also be used to override default functionality provided by Pickle's Acceptance object, though this is not
recommended and should only be used as a last resort as it could possible break in future releases.

#### World
The last interface exposed by Pickles are Worlds. To Pickles, Worlds are what allows communication with the browser.
Currently, Pickles only exposes a World that is based on Zombie.js. This World provides the only communcation layer with
Zombie.js.

The World is created in the constructor of the Acceptance object and exposed to the rest of the object as this.World. We
can modify the World in the constructor to add or replace default functionality:

    :::coffeescript
    #/tests/features/support/acceptance.coffee
    Acceptance = require('pickles').Acceptance

    class MyWebsiteAcceptance extends Acceptance

      ##
      # Creates a new acceptance object.
      #
      # @constructor
      # @this {MyWebsiteAcceptance}
      # @param {Function} callback The callback to be executed once creation is complete.
      #
      constructor: (callback) ->
        super(callback)
        @World.awesomeify = (something) ->
          @findElement("body").innerHTML = "AWESOMEIFYIED #{something}"

      ##
      # Does something SUPER AWESOMELY!
      #
      # @this {MyWebsiteAcceptance}
      # @param {String} something The thing to be done AWESOMELY!
      # @param {Function} callback The callback to be executed after completion.
      #
      mySuperAwesomeStep: (something, callback) ->
        @World.awesomeify(something)
        callback()

    module.exports = MyWebsiteAcceptance

If you find yourself modifying World a great deal, it would be wise to move the World implemention out into another file
and then extend the default World in the Acceptance constructor with your World object.

## Installation

    :::shell
    $ npm install pickles

Note: for Pickles to be useful, you should also have cucumber.js installed.

## License

MIT
