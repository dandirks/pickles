assert = require('assert')
World = require('./worlds/zombie-world').World

class Acceptance
  pages: {}

  getExpectedPath = (page) ->
    for sitePage, sitePath of @pages
      sitePageRegex = new RegExp("^#{sitePage}$")
      return page.replace(sitePageRegex, sitePath) if page.match(sitePageRegex)

    throw new Error("Page '#{page}' not defined")

  ##
  # Creates a new acceptance object.
  #
  # @constructor
  # @this {Acceptance}
  # @param {Function} callback The callback to be executed once creation is complete.
  #
  constructor: (callback) ->
    @World = Object.create(null)
    World.call(@World, callback)


  ### User Actions ###

  ##
  # Navigates the browser to a new page.
  #
  # @this {Acceptance}
  # @param {String} page The page to navigate to.
  # @param {Function} callback The callback to be executed after completion.
  #
  visit: (page, callback) ->
    @World.visit(getExpectedPath.call(@, page), callback)

  ##
  # Enters text into a text input field.
  #
  # @this {Acceptance}
  # @param {String} text The text to be entered.
  # @param {String} field The name of the field to enter text into.
  # @param {Function} callback The callback to be executed after completion.
  #
  enterTextInField: (text, field, callback) ->
    element = @World.findElement("input[name='#{field}'], textarea[name='#{field}']")
    @World.type(element, text, callback)

  ##
  # Clicks a button on the page.
  #
  # @this {Acceptance}
  # @param {String} button The name of the button to click.
  # @param {Function} callback The callback to be executed after completion.
  #
  clickButton: (button, callback) ->
    element = @World.findElement("button:contains('#{button}')")
    @World.click(element, callback)

  ##
  # Toggles a checkbox on or off.
  #
  # @this {Acceptance}
  # @param {String} state The state to change the checkbox to. Either "check" or "uncheck".
  # @param {String} checkbox The name of the checkbox to change.
  # @param {Function} callback The callback to be executed after completion.
  #
  toggleCheckboxState: (state, checkbox, callback) ->
    element = @World.findElement(checkbox, @World.by.label)
    if state is 'check'
      @World.check(element, callback)
    else
      @World.uncheck(element, callback)


  ### Verification ###

  ##
  # Verifies user is on a given page.
  #
  # @this {Acceptance}
  # @param {String} page The page that the user should be on.
  # @param {Function} callback The callback to be executed after completion.
  #
  verifyCurrentPage: (page, callback) ->
    assert.equal(@World.path(), getExpectedPath.call(@, page), "Not on '#{page}' page")
    callback()

  ##
  # Verifies text exists somewhere on the page.
  #
  # @this {Acceptance}
  # @param {String} text The text to look for on the page.
  # @param {Function} callback The callback to be executed after completion.
  #
  verifyTextOnPage: (text, callback) ->
    bodyText = @World.findElement('body').textContent
    assert.notEqual(bodyText.indexOf(text), -1, "Text '#{text}' not found on page")
    callback()

  ##
  # Verifies field doesn't exist on the page.
  #
  # @this {Acceptance}
  # @param {String} field The name of the field to look for.
  # @param {Function} callback The callback to be executed after completion.
  #
  verifyFieldDoesntExist: (field, callback) ->
    try
      element = @World.findElement("input[name='#{field}'], textarea[name='#{field}']")
    catch e
      return callback()
    callback.fail(new Error("Field '#{field}' found on page."))

  ##
  # Verifies text exists within field.
  #
  # @this {Acceptance}
  # @param {String} text The text to verify.
  # @param {String} field The name of the field to verify.
  # @param {Function} callback The callback to be executed after completion.
  #
  verifyTextInField: (text, field, callback) ->
    element = @World.findElement("input[name='#{field}'], textarea[name='#{field}']")
    assert.equal(element.value, text, "Text '#{text}' not found in field '#{field}'")
    callback()

  ##
  # Verifies checkbox state.
  #
  # @this {Acceptance}
  # @param {String} checkbox The name of the checkbox to verify.
  # @param {String} state The expected state of the checkbox. Either "checked" or "unchecked".
  # @param {Function} callback The callback to be executed after completion.
  #
  verifyCheckboxState: (checkbox, state, callback) ->
    element = @World.findElement(checkbox, @World.by.label)
    assert.equal(state is 'checked', element.checked, "Checkbox was not #{state}")
    callback()

  ##
  # Verifies an alert was seen on the page.
  #
  # @this {Acceptance}
  # @param {String} message The alert message that was seen.
  # @param {Function} callback The callback to be executed after completion.
  #
  verifyAlert: (message, callback) ->
    assert.ok(@World.alertedWithMessage(message), "Alert with text '#{message}' not seen on page.")
    callback()

module.exports = Acceptance
