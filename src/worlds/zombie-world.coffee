Browser = require('zombie')

module.exports.World = (callback) ->
  @browser = new Browser()
  @browser.site = "#{process.env.SERVER or 'localhost'}:#{process.env.PORT or '9500'}"

  @by =
    css: 'css'
    xpath: 'xpath'
    label: 'label'

  @visit = (url, callback) ->
    @browser.visit(url, callback)

  @path = ->
    return @browser.location.pathname

  @findElement = (selector, findBy=@by.css, findAll=false) ->
    if findBy is @by.css
      element = @browser[if findAll then 'queryAll' else 'query'].call(@browser, selector)
    else if findBy is @by.label
      findAll = false #this should never return multiple elements
      label = @browser.query("label:contains('#{selector}')")
      element = if label then @browser.query("##{label?.getAttribute('for')}") else undefined
    else
      element = @browser.xpath(selector)?.value
      element = element?[0] if not findAll
    if (not findAll and not element) or (findAll and not element.length)
      throw new Error("Element '#{selector}' not found using search method #{findBy}")
    return element

  @type = (field, value, callback) ->
    @browser.fill field, value, =>
      @browser.fire('keydown', field)
        .then =>
          @browser.fire('keyup', field)
        .then =>
          @browser.fire('keypress', field)
        .then(callback)

  @click = (element, callback) ->
    @browser.fire('click', element, callback)

  @check = (element, callback) ->
    @browser.check(element, callback)

  @uncheck = (element, callback) ->
    @browser.uncheck(element, callback)

  @alertedWithMessage = (message) ->
    return @browser.prompted(message)

  callback()
