module.exports = (Acceptance = require('./acceptance')) ->
  @World = Acceptance

  #Navigate to a specific page
  @defineStep(/^I visit the "?([^"]+)"? page$/, Acceptance::visit)
  #Verifies on given page
  @defineStep(/^I am on the "?([^"]+)"? page$/, Acceptance::verifyCurrentPage)
  #See text on page
  @defineStep(/^I see the text "?([^"]+)"?$/, Acceptance::verifyTextOnPage)
  #Click on a button
  @defineStep(/^I click the "?([^"]+)"? button$/, Acceptance::clickButton)
  #Verify field does not exist
  @defineStep(/^the "?([^"]+)"? field does not exist$/, Acceptance::verifyFieldDoesntExist)
  #Enter text into a field
  @defineStep(/^I enter "?([^"]+)"? in the "?([^"]+)"? field$/, Acceptance::enterTextInField)
  #Verify text exists within a field
  @defineStep(/^I see "?([^"]+)"? in the "?([^"]+)"? field$/, Acceptance::verifyTextInField)
  #Check/uncheck a checkbox
  @defineStep(/^I (check|uncheck) the "?([^"]*)"? checkbox$/, Acceptance::toggleCheckboxState)
  #Verify checkbox state
  @defineStep(/^I see the "?([^"]*)"? checkbox is (checked|unchecked)$/, Acceptance::verifyCheckboxState)
  #Verify browser alert
  @defineStep(/^I see an alert saying "?([^"]*)"?$/, Acceptance::verifyAlert)
